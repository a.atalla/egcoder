+++
title= "Deploy nextjs app to dokku"
date= 2022-09-16
draft= false
categories= ["Dokku", "Deployment", "Next.js"]
tags=["dokku", "heroku", "dokcker", "nextjs"]

[extra]
image="dokku-nextjs.png"
+++

Before you proceed you have to complete a dokku server installation like we did in the [previous article](/p/dokku-the-heroku-alternative-for-solo-developers)

## Create nextjs app

To create a new nextjs app, run the following command and follow the instruction to generate an app on your local development machine

```
npx create-next-app@latest

cd <app-name>

# Initialize git repo
git init
git add .
git commit -m "initial commit"

npm run dev
```

The nextjs dev server will start at port `3000`, now without touching the project we will deploy that app to our dokku server.

## Create and configure dokku app

- First we need to create a new app

```
dokku apps:create nextjs-demo
```

- To link the dokku app to our local project we need to add a remote git repo in our project, by default the new initialized git repo has no remote you can verify with `git remote -v`
  the dokku remote repo will be `<username>@<domain>:<app-name>`, notic that username has to be `dokku` which is the default admin on our dokku server.
  notice that you can add multiple remote to the same local repo, what i usually do is that i add a remote with the name `origin` to my github and a second remote with the name `dokku` for deployment

```
git remote add origin git@github.com:<github-username>/<github-repo>.git # (Optional but recommended)
git remote add dokku dokku@example.com:nextjs-demo

# Double check, to see the newly add remotes
git remote -v
```

- If your app need some environment variables, you may add it before deploy

```
dokku config:set nextjs-demo KEY1=VALUE1 KEY2=VALUE2 ....
```

- Push to dokku `master` branch to deploy your app, in my local machine git create the default branch with the name `main` so i have to explicitly tell to push it to remote `master`

```
git push dokku main:master
 ...
 ...

=====> Application deployed:
       http://nextjs-demo.example.com

To example.com:nextjs-demo
 * [new branch]      main -> master
```

Now your app is deployed and live at http://<app-name>.<domain>

## Add letsencrypt certificate and enable https

We all know that running producion website with `http` is not recommended, so we will add a free ssl certificate using `letsencrypt`,

- This is not builtin the dokku core and you had to install a plugin for it [dokku-letsencrypt](https://github.com/dokku/dokku-letsencrypt) , ssh into your dokku server and run teh following

```
sudo dokku plugin:install https://github.com/dokku/dokku-letsencrypt.git
```

- Add an email for letsencrypt

```
dokku config:set --global DOKKU_LETSENCRYPT_EMAIL=your@email.com
```

- Enable the certificate for your app

```
dokku letsencrypt:enable nextjs-demo
```

## Ports mapping

one thing to mention is that we know nextjs will run on port 3000, dokku expect the app to run on port 5000, if we checked the logs we will find that dokku automatically started the nextjs app at `5000`

```
dokku config:show nextjs-demo

=====> nextjs-demo env vars
DOKKU_APP_RESTORE:     1
DOKKU_APP_TYPE:        herokuish
DOKKU_PROXY_PORT:      80
DOKKU_PROXY_PORT_MAP:  http:80:5000
GIT_REV:               f231a49d1e3f7cc5f0ba83c24fad8a3ede8188d7

dokku logs nextjs-demo

2022-09-08T08:46:26.835065826Z app[web.1]:
2022-09-08T08:46:26.835116425Z app[web.1]: > nextjs-dokku-demo@0.1.0 start
2022-09-08T08:46:26.835128599Z app[web.1]: > next start
2022-09-08T08:46:26.835131699Z app[web.1]:
2022-09-08T08:46:27.151116325Z app[web.1]: ready - started server on 0.0.0.0:5000, url: http://localhost:5000
2022-09-08T08:46:27.161086702Z app[web.1]: info  - Loaded env from /app/.env
2022-09-08T08:46:28.194574764Z app[web.1]: info  - SWC minify release candidate enabled. https://nextjs.link/swcmin
```

if you have an app that run on different port than `5000` then you need to tell dokku about that by changing the proxy port config like this

```
# remove the default
dokky proxy:ports-remove nextjs-demo http:80:5000
dokku proxy:ports-remove nextjs-demo https:443:5000 # in case you setup ssl and https

# add the new
dokku proxy:ports-add nextjs-demo http:80:3000    # for http
dokku proxy:ports-add nextjs-demo https:443:3000  # for https
```
