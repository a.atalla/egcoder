+++
title= "Dokku the heroku alternative for solo developers"
date= 2022-08-27
draft= false
categories= ["Dokku", "Deployment"]
tags= ["dokku", "heroku", "dokcker"]
[extra]
image="dokku.png"
+++

## Heroku and free plan

In a blog post published at August 25, 2022 , Heroku general manager stated that they will stop offering free dynos `dyno is heroku computing unit` starting November, 28 2022, check the [blog post](https://blog.heroku.com/next-chapter) and [Remove free products plan](https://help.heroku.com/RSBRUH58/removal-of-heroku-free-product-plans-faq)

This decision will affect solo developers who use heroku to te st their ideas or host projects at early stage, and regarding to how easy it is to use heroku now we need an alternative

## Dokku

what is [dokku](https://dokku.com/) ?

> > An open source PAAS alternative to Heroku.
> > Dokku helps you build and manage the lifecycle of applications from building to scaling.

Dokku is an extensible, open source Platform as a Service that runs on a single server of your choice. Dokku supports building apps on the fly from a git push via either Dockerfile or by auto-detecting the language with Buildpacks, and then starts containers based on your built image. Using technologies such as nginx and cron, Web processes are automatically routed to, while background processes and automated cron tasks are also managed by Dokku.

## Getting started

### Minimum requirement to run dokku are:

- A fresh installation of `Ubuntu 18.04/20.04/22.04` or `Debian 10+ x64`.
- At least 1 GB of system memory.
- Attaching at least one domain name to your server ip, say `example.com`.

### Installation

- First ssh into your remote server.
- Download the `bootstrap.sh` installation script
  ```
  wget https://raw.githubusercontent.com/dokku/dokku/v0.28.1/bootstrap.sh
  ```
- Run the installer script by prefixing with `DOKKU_TaG=xxx` env variable to specify which version you want, latest is better offcourse

  ```
  sudo DOKKU_TAG=v0.28.1 bash bootstrap.sh
  ```

- This command will automatically use `apt` to update and install dependecies such as docker, then it will created the required containers, also a new user `dokku` will be created

- Next step is to add your public ssh key to be able to run dokku commands remotly, because dokku doesn't have a cli to install on your local machine like `heroku toolbel`

  To add your key, get the content from your local machine, the key filename usually called `id_rsa.pub`

  ```
  cat ~/.ssh/id_rsa.pub
  ```

  copy the file content and then on the remote server run this last command

  ```
  echo "your-public-key-contents-here" | dokku ssh-keys:add admin
  ```

  once this is done, you can execute `dokku` commands remotly from your locall machine like this

  ```
  ssh dokku@example.com apps:List
  ssh dokku@example.com domains:report --global
  ```

- Now we need set the domain as a dokku global domain.

  ```
  ssh dokku@example.com domains:add-global example.com


  # check your setup
  ssh dokku@example.com domains:report --global

  =====> Global domains information
        Domains global enabled:        true
        Domains global vhosts:         example.com
  ```

  with this done, any app you deploy will be available at `app-name.example.com` by default unless you add another domain specific to the app

  now you can create a new empty app like this

  ```
  # From remote server shell
  dokku apps:create my-first-application

  # From your local machine
  ssh dokku@<domain> apps:create my-second-app

  ```

In the next article i will show you how to deploy a NextJS application to your server
